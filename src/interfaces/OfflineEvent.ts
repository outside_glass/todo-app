export default interface OfflineEvent {
    isOffline: boolean;
}
