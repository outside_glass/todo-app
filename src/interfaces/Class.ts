export default interface Class {
    new(...args: any[]): any;
}
