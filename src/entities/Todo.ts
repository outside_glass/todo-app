import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import User from './User';
import TodoAttachment from './TodoAttachment';

@Entity()
export default class Todo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: User['id'];

    @Column()
    title: string;

    @Column({
        type: 'text',
    })
    description: string;

    @CreateDateColumn({
        name: 'createdAt',
        type: 'timestamp',
    })
    createdAt: string;

    @UpdateDateColumn({
        name: 'updatedAt',
        type: 'timestamp',
    })
    updatedAt: string;

    @Column({
        type: 'timestamp',
    })
    expiredAt: string;

    @ManyToOne(() => User, (user: User) => user.todos)
    @JoinColumn({ name: 'userId' })
    user: User;

    @OneToMany(() => TodoAttachment, (attachment: TodoAttachment) => attachment.todo, {
        onDelete: 'CASCADE',
    })
    attachments: TodoAttachment[];
}
