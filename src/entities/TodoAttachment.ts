import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Todo from './Todo';

@Entity()
export default class TodoAttachment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    key: string;

    @Column()
    todoId: Todo['id'];

    @CreateDateColumn({
        type: 'timestamp',
    })
    createdAt: string;

    @ManyToOne(() => Todo, (todo: Todo) => todo.attachments)
    @JoinColumn({ name: 'todoId' })
    todo: Todo;
}
