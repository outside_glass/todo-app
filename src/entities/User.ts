import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Todo from './Todo';

@Entity()
export default class User {
    public static readonly MAX_ALLOWED_TODOS: number = 32;

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    password: string;

    @Column()
    email: string;

    @OneToMany(() => Todo, (todo: Todo) => todo.user)
    todos: Todo[];

    constructor(username: string, password: string, email: string) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
}
