import { AbstractRepository, EntityRepository, FindManyOptions, FindOneOptions } from 'typeorm';
import Todo from '../entities/Todo';
import ContentBucket from '../services/ContentBucket';

@EntityRepository(Todo)
export default class TodoRepository extends AbstractRepository<Todo> {
    findAll(params: FindManyOptions<Todo> = {}): Promise<Todo[]> {
        return this.repository.find(params);
    }

    findOne(params: FindOneOptions<Todo> = {}): Promise<Todo> {
        return this.repository.findOne(params);
    }

    save(data: Partial<Todo>): Promise<Todo> {
        const todo = this.repository.create(data);

        return this.repository.save(todo);
    }

    async update(criteria: Partial<Todo>, data: Partial<Omit<Todo, 'id'>>): Promise<void> {
        await this.repository.update(criteria, data);
    }

    async delete(criteria: FindManyOptions<Todo>): Promise<void> {
        const todos = await this.repository.find({
            relations: ['attachments'],
            where: criteria,
        });

        for (const todo of todos) {
            await this.repository.remove(todo);

            if(!process.env.IS_OFFLINE) {
                for (const attachment of todo.attachments) {
                    await ContentBucket.deleteObject(attachment.key);
                }
            }
        }
    }
}
