import TodoAttachment from '../entities/TodoAttachment';
import { AbstractRepository, EntityRepository, FindManyOptions, FindOneOptions } from 'typeorm';
import ContentBucket from '../services/ContentBucket';

@EntityRepository(TodoAttachment)
export default class TodoAttachmentRepository extends AbstractRepository<TodoAttachment> {
    findOne(criteria: FindOneOptions<TodoAttachment>): Promise<TodoAttachment> {
        return this.repository.findOne(criteria);
    }

    save(data: Partial<TodoAttachment>): Promise<TodoAttachment> {
        const todoAttachment = this.repository.create(data);

        return this.repository.save(todoAttachment);
    }

    async delete(criteria: FindManyOptions<TodoAttachment>): Promise<void> {
        const attachments = await this.repository.find({
            where: criteria,
        });

        await this.repository.remove(attachments);

        if(!process.env.IS_OFFLINE) {
            for(const { key } of attachments) {
                await ContentBucket.deleteObject(key);
            }
        }
    }
}
