import { AbstractRepository, EntityRepository, FindManyOptions, FindOneOptions } from 'typeorm';
import User from '../entities/User';

@EntityRepository(User)
export default class UserRepository extends AbstractRepository<User> {
    findAll(conditions: FindManyOptions<User>): Promise<User[]> {
        return this.repository.find(conditions);
    }

    findOne(conditions: FindOneOptions<User>): Promise<User> {
        return this.repository.findOne(conditions);
    }

    save(data: Partial<User>): Promise<User> {
        const user = this.repository.create(data);

        return this.repository.save(user);
    }
}
