import { MiddlewareObject } from 'middy';
import Joi from '@hapi/joi';
import createHttpError from 'http-errors';

export interface EventValidationSchema {
    body?: Joi.ObjectSchema;
    pathParameters?: Joi.ObjectSchema;
}

export function joiValidator<TEvent extends any, TResponse = any>(
    {
        body,
        pathParameters,
    }: EventValidationSchema,
): MiddlewareObject<TEvent, TResponse> {
    return {
        before: ({ event }, next) => {
            const schema = Joi.object({
                ...(body && { body: body.required() }),
                ...(pathParameters && { pathParameters: pathParameters.required() }),
            })
                .unknown(true);

            const { error, value } = schema.validate(event, {
                abortEarly: false,
                convert: true,
            });

            if(error) {
                const details = error.details.map((detail) => detail.message);

                throw new createHttpError.BadRequest(details.join(', '));
            }

            Object.assign(event, value);

            next();
        },
    };
}
