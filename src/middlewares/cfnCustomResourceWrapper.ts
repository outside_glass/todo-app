import { MiddlewareObject } from 'middy';
import { CloudFormationCustomResourceEvent, Context } from 'aws-lambda';
import axios from 'axios';
import CfnResponse from '../interfaces/CfnResponse';

async function sendResponse(
    {
        event,
        context,
        response,
    }: {
        event: CloudFormationCustomResourceEvent;
        context: Context,
        response: CfnResponse;
    },
) {
    const payload = {
        Status: response.status,
        Reason: response.reason,
        PhysicalResourceId: context.logGroupName || event.LogicalResourceId,
        LogicalResourceId: event.LogicalResourceId,
        StackId: event.StackId,
        RequestId: event.RequestId,
        Data: response.data,
    };

    console.log(`Payload: ${JSON.stringify(payload, undefined, 2)}`);

    console.log('Event', event);
    console.log(`PUTting payload to ${event.ResponseURL}`);

    await axios.put(event.ResponseURL, JSON.stringify(payload));
}

export default function cfnCustomResourceWrapper(
    opts: {
        isDisabled?: boolean;
    } = {
        isDisabled: false,
    },
): MiddlewareObject<any, CfnResponse> {
    if (opts.isDisabled) {
        return {
            after: (handler, next) => next(),
        };
    }

    return {
        after: async (handler) => {
            await sendResponse({
                event: handler.event,
                response: handler.response,
                context: handler.context,
            });
        },
        onError: async (handler) => {
            console.error(handler.error);
            await sendResponse({
                event: handler.event,
                response: {
                    status: 'FAILED',
                    reason: handler.error.message,
                },
                context: handler.context,
            });
        },
    };
}
