import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

// tslint:disable-next-line:class-name
export class changeTimestampTypesInTodoTable1580139269885 implements MigrationInterface {
    private readonly todoTable: string = 'todo';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'createdAt',
                type: 'integer',
            }),
            new TableColumn({
                name: 'createdAt',
                type: 'timestamp',
                default: 'now()',
            }),
        );

        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'updatedAt',
                type: 'integer',
            }),
            new TableColumn({
                name: 'updatedAt',
                type: 'timestamp',
                default: 'now()',
            }),
        );

        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'expireAt',
                type: 'integer',
            }),
            new TableColumn({
                name: 'expiredAt',
                type: 'timestamp',
                isNullable: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'createdAt',
                type: 'timestamp',
            }),
            new TableColumn({
                name: 'createdAt',
                type: 'integer',
            }),
        );

        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'updatedAt',
                type: 'timestamp',
            }),
            new TableColumn({
                name: 'updatedAt',
                type: 'integer',
            }),
        );

        await queryRunner.changeColumn(this.todoTable,
            new TableColumn({
                name: 'expireAt',
                type: 'timestamp',
            }),
            new TableColumn({
                name: 'expiredAt',
                type: 'integer',
            }),
        );
    }

}
