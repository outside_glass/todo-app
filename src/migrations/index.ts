import RequireContext = __WebpackModuleApi.RequireContext;
import Class from '../interfaces/Class';

let context: RequireContext | undefined;

// To prevent 'require.context is not a function' error when using typeorm cli
let exported: Class[] | string[];

try {
    context = require.context('./', true, /^\.\/(\d+)-(.+)\.ts$/);
} catch (err) {
}

if (context) {
    const migrations = [];

    for (const key of context.keys()) {
        migrations.push(Object.values(context(key) as { [className: string]: Class })[0]);
    }

    exported = migrations;
} else {
    exported = ['src/migrations/**/*.ts'];
}

export default exported;
