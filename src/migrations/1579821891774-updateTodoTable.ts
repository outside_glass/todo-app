import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey, TableIndex } from 'typeorm';

// tslint:disable-next-line:class-name
export class updateTodoTable1579821891774 implements MigrationInterface {
    private readonly todoTable: string = 'todo';
    private readonly userTable: string = 'user';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn(this.todoTable, new TableColumn({
            name: 'expireAt',
            type: 'integer',
            isNullable: true,
        }));

        await queryRunner.addColumn(this.todoTable, new TableColumn({
            name: 'userId',
            type: 'integer',
            isNullable: false,
        }));

        await queryRunner.createIndices(this.todoTable, [
            new TableIndex({
                columnNames: [
                    'expireAt',
                ],
            }),
            new TableIndex({
                columnNames: [
                    'userId',
                ],
            }),
        ]);

        await queryRunner.createForeignKey(this.todoTable, new TableForeignKey({
            columnNames: [
                'userId',
            ],
            referencedTableName: this.userTable,
            referencedColumnNames: [
                'id',
            ],
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropForeignKey(this.todoTable, new TableForeignKey({
            columnNames: [
                'userId',
            ],
            referencedTableName: this.userTable,
            referencedColumnNames: [
                'id',
            ],
        }));

        await queryRunner.dropColumn(this.todoTable, new TableColumn({
            name: 'expireAt',
            type: 'integer',
        }));

        await queryRunner.dropColumn(this.todoTable, new TableColumn({
            name: 'userId',
            type: 'integer',
        }));
    }

}
