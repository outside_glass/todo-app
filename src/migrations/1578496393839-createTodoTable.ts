import { MigrationInterface, QueryRunner, Table } from 'typeorm';

// tslint:disable-next-line:class-name
export class createTodoTable1578496393839 implements MigrationInterface {
    private readonly tableName: string = 'todo';

    public async up(queryRunner: QueryRunner): Promise<any> {
        const todoTable = new Table({
            name: this.tableName,
            columns: [
                {
                    name: 'id',
                    type: 'serial',
                    isPrimary: true,
                },
                {
                    name: 'title',
                    type: 'varchar',
                    isNullable: false,
                },
                {
                    name: 'description',
                    type: 'text',
                    isNullable: true,
                },
                {
                    name: 'createdAt',
                    type: 'int',
                    isNullable: false,
                },
                {
                    name: 'updatedAt',
                    type: 'int',
                    isNullable: false,
                },
            ],
            indices: [
                {
                    columnNames: ['title', 'description'],
                    isFulltext: true,
                },
                {
                    columnNames: ['createdAt'],
                },
                {
                    columnNames: ['updatedAt'],
                },
            ],
        });

        await queryRunner.createTable(todoTable);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(this.tableName);
    }

}
