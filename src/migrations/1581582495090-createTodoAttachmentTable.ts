import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

// tslint:disable-next-line:class-name
export class createTodoAttachmentTable1581582495090 implements MigrationInterface {
    private readonly todoAttachmentTable: string = 'todo_attachment';
    private readonly todoTable: string = 'todo';

    public async up(queryRunner: QueryRunner): Promise<any> {
        const todoAttachmentTable = new Table({
            name: this.todoAttachmentTable,
            columns: [
                {
                    name: 'id',
                    type: 'serial',
                    isPrimary: true,
                },
                {
                    name: 'key',
                    type: 'varchar',
                    isNullable: false,
                },
                {
                    name: 'todoId',
                    type: 'int',
                    isNullable: false,
                },
                {
                    name: 'createdAt',
                    default: 'now()',
                    type: 'timestamp',
                    isNullable: false,
                },
            ],
            indices: [
                new TableIndex({
                    columnNames: ['key'],
                    isUnique: true,

                }),
                new TableIndex({
                    columnNames: ['todoId'],
                }),
            ],
            foreignKeys: [
                new TableForeignKey({
                    columnNames: ['todoId'],
                    referencedTableName: this.todoTable,
                    referencedColumnNames: ['id'],
                    onDelete: 'CASCADE',
                }),
            ],
        });

        await queryRunner.createTable(todoAttachmentTable);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable(this.todoAttachmentTable);
    }

}
