import TodoAttachment from '../../entities/TodoAttachment';

export default interface TodoAttachmentView {
    id: TodoAttachment['id'];
    todoId: TodoAttachment['todoId'];
    createdAt: TodoAttachment['createdAt'];
}
