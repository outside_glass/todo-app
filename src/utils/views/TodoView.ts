import Todo from '../../entities/Todo';
import TodoAttachmentView from './TodoAttachmentView';

export default interface TodoView {
    id: Todo['id'];
    title: Todo['title'];
    description: Todo['description'];
    createdAt: Todo['createdAt'];
    updatedAt: Todo['updatedAt'];
    expiredAt: Todo['expiredAt'];
    attachments: TodoAttachmentView[];
}
