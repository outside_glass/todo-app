import TodoAttachment from '../../entities/TodoAttachment';
import TodoAttachmentView from '../views/TodoAttachmentView';

export default function createTodoAttachmentView(attachment: TodoAttachment): TodoAttachmentView {
    return {
        id: attachment.id,
        todoId: attachment.todoId,
        createdAt: attachment.createdAt,
    };
}
