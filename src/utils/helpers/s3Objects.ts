import uuid from 'uuid';
import path from 'path';
import BucketPrefixes from '../enums/BucketPrefixes';
import User from '../../entities/User';
import Todo from '../../entities/Todo';

interface PathParameters {
    prefix: BucketPrefixes;
    userId: User['id'];
    todoId: Todo['id'];
    name: string;
}

export function generatePath(
    {
        prefix,
        userId,
        todoId,
        name,
    }: PathParameters,
) {
    return path.resolve(
        prefix,
        String(+userId),
        String(todoId),
        `${uuid.v4()}${path.extname(name)}`);
}

export function parsePath(path: string): Omit<PathParameters, 'prefix'> {
    const [full, prefix, userId, todoId, name] = path.match(/^\/(.+)\/(.+)\/(.+)\/(.+)$/);

    return {
        name,
        userId: +userId,
        todoId: +todoId,
    };
}
