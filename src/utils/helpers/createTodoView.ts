import Todo from '../../entities/Todo';
import createTodoAttachmentView from './createTodoAttachmentView';
import TodoView from '../views/TodoView';

export default function createTodoView(todo: Todo): TodoView {
    return {
        id: todo.id,
        title: todo.title,
        description: todo.description,
        createdAt: todo.createdAt,
        updatedAt: todo.updatedAt,
        expiredAt: todo.expiredAt,
        attachments: todo.attachments.map((attachment) => createTodoAttachmentView(attachment)),
    };
}
