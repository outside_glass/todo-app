export declare class QueryFailedError extends Error {
    message: string;
    name: 'QueryFailedError';
    length: number;
    severity: 'ERROR';
    code: string;
    detail: string;
    hint: string;
    position: number;
    internalPosition: number;
    internalQuery: string;
    where: string;
    schema: string;
    table: string;
    column: string;
    dataType: string;
    constraint: string;
    file: string;
    line: string;
    routine: string;
    query: string;
    parameters: string[];

    constructor(query: string, parameters: any[] | undefined, driverError: any);
}
