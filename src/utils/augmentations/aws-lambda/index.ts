import AuthorizerContext from '../../../interfaces/AuthorizerContext';

declare module 'aws-lambda' {
    export interface AuthResponseContext extends AuthorizerContext {}
}
