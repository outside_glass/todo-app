import { HttpError } from 'http-errors';
import OfflineEvent from '../interfaces/OfflineEvent';
import { QueryFailedError } from './augmentations/typeorm';

export function isHttpError(error: unknown): error is HttpError {
    return error.hasOwnProperty('statusCode');
}

export function isOfflineEvent(event: unknown): event is OfflineEvent {
    return event.hasOwnProperty('isOffline');
}

export function isQueryFailedError(error: unknown): error is QueryFailedError {
    return error.hasOwnProperty('query')
        && error.hasOwnProperty('detail')
        && error.hasOwnProperty('code')
        && error.hasOwnProperty('name')
        && error['name'] === 'QueryFailedError';
}
