import AWS, { AWSError } from 'aws-sdk';
import { SendMessageRequest, SendMessageResult } from 'aws-sdk/clients/sqs';
import { PromiseResult } from 'aws-sdk/lib/request';

export default class MailQueue {
    private static instance: AWS.SQS;
    private static queueURL: string;

    private static async getInstance(): Promise<AWS.SQS> {
        if (!this.queueURL) {
            this.instance = new AWS.SQS();

            const { QueueUrl } = await this.instance.getQueueUrl({
                QueueName: process.env.MAIL_QUEUE_NAME,
            }).promise();

            this.queueURL = QueueUrl;
        }

        return this.instance;
    }

    public static async sendMessage(
        options: Omit<SendMessageRequest, 'QueueUrl'>,
    ): Promise<PromiseResult<SendMessageResult, AWSError>> {

        const instance = await this.getInstance();

        return await instance.sendMessage({
            ...options,
            QueueUrl: this.queueURL,
        }).promise();
    }
}
