import { DeleteObjectOutput, PresignedPost } from 'aws-sdk/clients/s3';
import { PromiseResult } from 'aws-sdk/lib/request';
import { AWSError } from 'aws-sdk';
import S3 = require('aws-sdk/clients/s3');

export default class ContentBucket {
    private static readonly s3: S3 = new S3();

    static createPresignedPost(key: string): PresignedPost {
        return this.s3.createPresignedPost({
            Bucket: process.env.CONTENT_BUCKET,
            Expires: 180,
            Fields: {
                Key: key,
            },
            Conditions: [
                ['content-length-range', 1024, 1048576],
            ],
        });
    }

    static createPresignedGet(key: string): string {
        return this.s3.getSignedUrl('getObject', {
            Bucket: process.env.CONTENT_BUCKET,
            Key: key,
            Expires: 600,
        });
    }

    static deleteObject(key: string): Promise<PromiseResult<DeleteObjectOutput, AWSError>> {
        return this.s3.deleteObject({
            Bucket: process.env.CONTENT_BUCKET,
            Key: key,
        }).promise();
    }
}
