import { Context, SQSEvent } from 'aws-lambda';
import AWS from 'aws-sdk';
import middy from 'middy';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';

interface TodoParts {
    title: string;
    expiredAt: string;
}

async function rawHandler(event: SQSEvent, context: Context): Promise<void> {
    try {
        for(const record of event.Records) {
            const address = record.messageAttributes.address.stringValue;
            const todos = JSON.parse(record.body);

            await sendMessage(address, todos);
        }

        context.succeed(event);
    } catch (err) {
        console.log(err);

        context.fail(err);
    }
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop());

async function sendMessage(address: string, todos: TodoParts[]): Promise<void> {
    const ses = new AWS.SES();

    await ses.sendEmail({
        Message: {
            Subject: {
                Data: 'Expired todos',
            },
            Body: {
                Text: {
                    Data: formatTodos(todos),
                },
            },
        },
        Source: process.env.SES_SOURCE,
        Destination: {
            ToAddresses: [
                address,
            ],
        },
    }).promise();
}

function formatTodos(todos: TodoParts[]): string {
    return todos.map(({ title, expiredAt }) => `${expiredAt}: ${title}`)
        .join('\n');
}
