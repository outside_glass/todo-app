import Response from '../../../interfaces/Response';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import TodoRepository from '../../../repos/TodoRepository';
import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import middy from 'middy';
import { cors, doNotWaitForEmptyEventLoop, httpErrorHandler, jsonBodyParser } from 'middy/middlewares';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import { isHttpError } from '../../../utils/typeguards';

interface PathParams {
    id: number;
}

async function rawHandler(event: APIGatewayEvent<any, PathParams>): Promise<Response> {
    try {
        const {
            pathParameters: { id },
            requestContext: { authorizer: { principalId: userId } },
        } = event;

        await establishDbConnection();

        const repo = getCustomRepository(TodoRepository);

        await repo.update({
            id,
            userId,
        }, event.body);

        return {
            statusCode: 204,
            body: JSON.stringify({
                message: 'Successfully updated',
            }),
        };
    } catch (err) {
        console.error(err);

        if(isHttpError(err)) {
            return {
                statusCode: err.status,
                body: JSON.stringify({
                    message: err.message,
                }),
            };
        }

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    pathParameters: Joi.object({
        id: Joi.number().required(),
    }),
    body: Joi.object({
        id: Joi.forbidden(),
        userId: Joi.forbidden(),
        title: Joi.string()
            .max(255),
        description: Joi.string(),
        createdAt: Joi.forbidden(),
        updatedAt: Joi.forbidden(),
        expiredAt: Joi.date().iso().greater('now').optional(),
        user: Joi.forbidden(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(jsonBodyParser())
    .use(httpErrorHandler())
    .use(cors())
    .use(joiValidator(eventSchema));
