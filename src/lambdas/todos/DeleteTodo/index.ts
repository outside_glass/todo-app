import Response from '../../../interfaces/Response';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import TodoRepository from '../../../repos/TodoRepository';
import middy from 'middy';
import { cors, doNotWaitForEmptyEventLoop, httpErrorHandler, httpEventNormalizer } from 'middy/middlewares';
import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';

interface PathParams {
    id: number;
}

async function rawHandler(event: APIGatewayEvent<any, PathParams>): Promise<Response> {
    try {
        const {
            pathParameters: { id },
            requestContext: { authorizer: { principalId: userId } },
        } = event;

        await establishDbConnection();

        const todoRepo = getCustomRepository(TodoRepository);

        await todoRepo.delete({
            where: {
                id,
                userId,
            },
        });

        return {
            statusCode: 204,
            body: JSON.stringify({
                message: 'Successfully deleted',
            }),
        };
    } catch (err) {
        console.error(err);

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    pathParameters: Joi.object({
        id: Joi.number().required(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    .use(cors())
    .use(joiValidator(eventSchema));
