import Response from '../../../interfaces/Response';
import { getCustomRepository } from 'typeorm';
import TodoRepository from '../../../repos/TodoRepository';
import middy from 'middy';
import { cors, doNotWaitForEmptyEventLoop, httpErrorHandler } from 'middy/middlewares';
import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import establishDbConnection from '../../../services/DbConnection';
import createTodoView from '../../../utils/helpers/createTodoView';

async function rawHandler(event: APIGatewayEvent): Promise<Response> {
    const { principalId: userId } = event.requestContext.authorizer;

    await establishDbConnection();

    const todoRepo = getCustomRepository(TodoRepository);

    const todos = await todoRepo.findAll({
        where: {
            userId,
        },
        relations: ['attachments'],
    });

    return {
        statusCode: 200,
        body: JSON.stringify(todos.map((todo) => createTodoView(todo))),
    };
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(httpErrorHandler())
    .use(cors());
