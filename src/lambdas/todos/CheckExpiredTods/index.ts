import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository, Raw } from 'typeorm';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';
import UserRepository from '../../../repos/UserRepository';
import MailQueue from '../../../services/MailQueue';
import middy = require('middy');
import moment = require('moment');

async function rawHandler(): Promise<void> {
    await establishDbConnection();

    const userRepo = getCustomRepository(UserRepository);

    const usersWithExpiredTodos = await userRepo.findAll({
        relations: ['todos'],
        where: {
            todos: {
                expiredAt: Raw((column) => `${column} < NOW() and ${column} is not null`),
            },
        },
    });

    for (const user of usersWithExpiredTodos) {
        const expiredTodos = user.todos.filter((todo) => moment(todo.expiredAt) < moment());

        const body = JSON.stringify(
            expiredTodos.map(({ title, expiredAt }) => ({ title, expiredAt })),
        );

        await MailQueue.sendMessage({
            MessageBody: body,
            MessageAttributes: {
                address: {
                    DataType: 'String',
                    StringValue: user.email,
                },
            },
        });
    }
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop());
