import Response from '../../../interfaces/Response';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import TodoRepository from '../../../repos/TodoRepository';
import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import middy from 'middy';
import { cors, doNotWaitForEmptyEventLoop, httpErrorHandler, jsonBodyParser } from 'middy/middlewares';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import User from '../../../entities/User';
import { isQueryFailedError } from '../../../utils/typeguards';
import createTodoView from '../../../utils/helpers/createTodoView';

async function rawHandler(event: APIGatewayEvent): Promise<Response> {
    try {
        await establishDbConnection();

        const {
            body: payload,
            requestContext: { authorizer: { principalId: userId } },
        } = event;

        const repo = getCustomRepository(TodoRepository);

        const userTodos = await repo.findAll({
            where: {
                userId,
            },
        });

        if(userTodos.length === User.MAX_ALLOWED_TODOS) {
            return {
                statusCode: 403,
                body: 'Todo limit reached',
            };
        }

        const savedTodo = await repo.save({
            ...payload,
            userId,
        });

        return {
            statusCode: 201,
            body: JSON.stringify(createTodoView(savedTodo)),
        };
    } catch (err) {
        console.error(err);

        if(isQueryFailedError(err)) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: err.detail,
                }),
            };
        }

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    body: Joi.object({
        id: Joi.forbidden(),
        userId: Joi.forbidden(),
        title: Joi.string().max(255).required(),
        description: Joi.string().required(),
        createdAt: Joi.forbidden(),
        updatedAt: Joi.forbidden(),
        expiredAt: Joi.date().iso().greater('now').optional(),
        user: Joi.forbidden(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(jsonBodyParser())
    .use(httpErrorHandler())
    .use(cors())
    .use(joiValidator(eventSchema));
