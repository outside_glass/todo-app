import UserRepository from '../../../repos/UserRepository';
import Response from '../../../interfaces/Response';
import { getCustomRepository } from 'typeorm';
import establishDbConnection from '../../../services/DbConnection';
import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import middy from 'middy';
import { cors, doNotWaitForEmptyEventLoop, httpErrorHandler, jsonBodyParser } from 'middy/middlewares';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import { isQueryFailedError } from '../../../utils/typeguards';

async function rawHandler(event: APIGatewayEvent): Promise<Response> {
    await establishDbConnection();

    try {
        const userRepo = getCustomRepository(UserRepository);

        await userRepo.save(event.body);

        return {
            statusCode: 201,
            body: JSON.stringify({
                message: 'Successfully registered',
            }),
        };
    } catch (err) {
        console.error(err);

        if(isQueryFailedError(err)) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: err.detail,
                }),
            };
        }

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    body: Joi.object({
        id: Joi.forbidden(),
        username: Joi.string().required(),
        password: Joi.string().required(),
        email: Joi.string().email().required(),
        expiredAt: Joi.number().optional(),
        todos: Joi.forbidden(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(jsonBodyParser())
    .use(httpErrorHandler())
    .use(cors())
    .use(joiValidator(eventSchema));
