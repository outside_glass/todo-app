import { CustomAuthorizerEvent, PolicyDocument } from 'aws-lambda';
import UserRepository from '../../../repos/UserRepository';
import { getCustomRepository } from 'typeorm';
import establishDbConnection from '../../../services/DbConnection';
import createHttpError from 'http-errors';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';
import middy from 'middy';
import User from '../../../entities/User';

interface Credentials {
    username: string;
    password: string;
}

interface AuthorizerResponse {
    principalId: User['id'] | undefined;
    policyDocument: PolicyDocument;
}

async function rawHandler(event: CustomAuthorizerEvent): Promise<AuthorizerResponse> {
    try {
        const credentials = parseCredentials(event.authorizationToken);

        await establishDbConnection();
        const userRepo = getCustomRepository(UserRepository);

        const user = await userRepo.findOne({
            where: credentials,
        });

        if(!user) {
            throw new createHttpError.Forbidden('Incorrect credentials');
        }

        return generatePolicy(user.id, 'allow', '*');
    } catch (err) {
        return generatePolicy(undefined, 'deny', '*');
    }
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop());

function generatePolicy(
    principalId: AuthorizerResponse['principalId'],
    effect: 'allow' | 'deny',
    methodArn: string,
): AuthorizerResponse {
    const policyDocument: PolicyDocument = {
        Version: '2012-10-17',
        Statement: [
            {
                Effect: effect,
                Action: 'execute-api:Invoke',
                Resource: methodArn,
            },
        ],
    };

    return {
        principalId,
        policyDocument,
    };
}

function parseCredentials(header: string): Credentials {
    if(!header || !header.includes('Basic ')) {
        throw new createHttpError.BadRequest('Authorization required');
    }

    const token = header.split(' ')[1];

    const credentials = Buffer.from(token, 'base64').toString();

    if(!credentials.includes(':')) {
        throw new createHttpError.BadRequest('Invalid credentials');
    }

    const [username, password] = credentials.split(':');

    return {
        username,
        password,
    };
}
