import { Callback, Context, S3Event } from 'aws-lambda';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import TodoAttachmentRepository from '../../../repos/TodoAttachmentRepository';
import { parsePath } from '../../../utils/helpers/s3Objects';
import middy from 'middy';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';

/**
 * Saves attachment in database on s3:ObjectCreated:* event
 * @param Records
 * @param context
 * @param callback
 */
async function rawHandler({ Records }: S3Event, context: Context, callback: Callback): Promise<void> {
    try {
        await establishDbConnection();

        const todoAttachmentRepo = getCustomRepository(TodoAttachmentRepository);

        for (const record of Records) {
            const { s3: { object: { key } } } = record;

            const { todoId } = parsePath(key);

            await todoAttachmentRepo.save({
                todoId,
                key,
            });
        }

        callback();
    } catch (err) {
        console.log(err);

        callback(err);
    }
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop());
