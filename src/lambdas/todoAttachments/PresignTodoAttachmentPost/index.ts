import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import Response from '../../../interfaces/Response';
import {
    cors,
    doNotWaitForEmptyEventLoop,
    httpErrorHandler,
    httpEventNormalizer,
    jsonBodyParser,
} from 'middy/middlewares';
import middy from 'middy';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import ContentBucket from '../../../services/ContentBucket';
import TodoRepository from '../../../repos/TodoRepository';
import { generatePath } from '../../../utils/helpers/s3Objects';
import createHttpError from 'http-errors';
import { isHttpError } from '../../../utils/typeguards';
import BucketPrefixes from '../../../utils/enums/BucketPrefixes';
import Todo from '../../../entities/Todo';

interface Body {
    filename: string;
}

interface PathParams {
    todoId: Todo['id'];
}

async function rawHandler(event: APIGatewayEvent<Body, PathParams>): Promise<Response> {
    try {
        await establishDbConnection();

        const todoRepo = getCustomRepository(TodoRepository);

        const {
            requestContext: { authorizer: { principalId: userId } },
            pathParameters: { todoId },
            body: { filename },
        } = event;

        const todo = await todoRepo.findOne({
            relations: ['user'],
            where: {
                id: todoId,
                user: {
                    id: userId,
                },
            },
        });

        if(!todo) {
            throw new createHttpError.NotFound();
        }

        const key = generatePath({
            userId,
            todoId,
            prefix: BucketPrefixes.TODO_ATTACHMENT,
            name: filename,
        });

        const presignedUrl = ContentBucket.createPresignedPost(key);

        return {
            statusCode: 201,
            body: JSON.stringify(presignedUrl),
        };
    } catch (err) {
        console.error(err);

        if(isHttpError(err)) {
            return {
                statusCode: err.status,
                body: JSON.stringify({
                    message: err.message,
                }),
            };
        }

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    body: Joi.object({
        filename: Joi.string().required(),
    }),
    pathParameters: Joi.object({
        todoId: Joi.number().required(),
        attachmentId: Joi.number().required(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(jsonBodyParser())
    .use(httpErrorHandler())
    .use(httpEventNormalizer())
    .use(cors())
    .use(joiValidator(eventSchema));
