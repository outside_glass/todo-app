import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import Response from '../../../interfaces/Response';
import middy from 'middy';
import { doNotWaitForEmptyEventLoop, httpErrorHandler, httpEventNormalizer, jsonBodyParser } from 'middy/middlewares';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import TodoAttachmentRepository from '../../../repos/TodoAttachmentRepository';
import Todo from '../../../entities/Todo';
import TodoAttachment from '../../../entities/TodoAttachment';

interface PathParams {
    todoId: Todo['id'];
    attachmentId: TodoAttachment['id'];
}

/**
 * Removes attachment from database and from S3 Bucket
 * @param event
 */
async function rawHandler(event: APIGatewayEvent<object, PathParams>): Promise<Response> {
    try {
        await establishDbConnection();

        const todoAttachmentRepo = getCustomRepository(TodoAttachmentRepository);

        const {
            requestContext: { authorizer: { principalId: userId } },
            pathParameters: { todoId, attachmentId },
        } = event;

        await todoAttachmentRepo.delete({
            relations: ['todo', 'todo.user'],
            where: {
                id: attachmentId,
                todo: {
                    id: todoId,
                    user: {
                        id: userId,
                    },
                },
            },
        });

        return {
            statusCode: 204,
            body: JSON.stringify({
                message: 'Successfully deleted',
            }),
        };
    } catch (err) {
        console.error(err);

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    pathParameters: Joi.object({
        todoId: Joi.number().required(),
        attachmentId: Joi.number().required(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(httpEventNormalizer())
    .use(httpErrorHandler())
    .use(jsonBodyParser())
    .use(joiValidator(eventSchema));
