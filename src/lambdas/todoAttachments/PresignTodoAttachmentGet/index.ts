import APIGatewayEvent from '../../../interfaces/APIGatewayEvent';
import middy from 'middy';
import { doNotWaitForEmptyEventLoop, httpEventNormalizer } from 'middy/middlewares';
import { EventValidationSchema, joiValidator } from '../../../middlewares/joiValidator';
import Joi from '@hapi/joi';
import establishDbConnection from '../../../services/DbConnection';
import { getCustomRepository } from 'typeorm';
import createHttpError from 'http-errors';
import Response from '../../../interfaces/Response';
import { isHttpError } from '../../../utils/typeguards';
import ContentBucket from '../../../services/ContentBucket';
import TodoAttachmentRepository from '../../../repos/TodoAttachmentRepository';
import Todo from '../../../entities/Todo';
import TodoAttachment from '../../../entities/TodoAttachment';

interface PathParams {
    todoId: Todo['id'];
    attachmentId: TodoAttachment['id'];
}

async function rawHandler(event: APIGatewayEvent<any, PathParams>): Promise<Response> {
    try {
        await establishDbConnection();

        const {
            requestContext: { authorizer: { principalId: userId } },
            pathParameters: { todoId, attachmentId },
        } = event;

        const todoRepo = getCustomRepository(TodoAttachmentRepository);

        const attachment = await todoRepo.findOne({
            relations: ['todo', 'todo.user'],
            where: {
                id: attachmentId,
                todo: {
                    id: todoId,
                    user: {
                        id: userId,
                    },
                },
            },
        });

        if(!attachment) {
            throw new createHttpError.NotFound();
        }

        const presignedUrl = ContentBucket.createPresignedGet(attachment.key);

        return {
            statusCode: 200,
            body: presignedUrl,
        };
    } catch (err) {
        console.error(err);

        if(isHttpError(err)) {
            return {
                statusCode: err.status,
                body: JSON.stringify({
                    message: err.message,
                }),
            };
        }

        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'Unexpected error',
            }),
        };
    }
}

const eventSchema: EventValidationSchema = {
    pathParameters: Joi.object({
        todoId: Joi.number().required(),
        attachmentId: Joi.number().required(),
    }),
};

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop())
    .use(httpEventNormalizer())
    .use(joiValidator(eventSchema));
