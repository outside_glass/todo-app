import { Callback, Context } from 'aws-lambda';
import establishDbConnection from '../../../services/DbConnection';
import middy from 'middy';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';

async function rawHandler(event: object, context: Context, callback: Callback): Promise<void> {
    try {
        const connection = await establishDbConnection();

        await connection.undoLastMigration();

        callback();
    } catch (err) {
        console.error(err.message);

        callback(err);
    }
}

export const handler = middy(rawHandler)
    .use(doNotWaitForEmptyEventLoop());
