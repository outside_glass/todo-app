import 'source-map-support/register';
import establishDbConnection from '../../../services/DbConnection';
import cfnCustomResourceWrapper from '../../../middlewares/cfnCustomResourceWrapper';
import { doNotWaitForEmptyEventLoop } from 'middy/middlewares';
import middy from 'middy';
import CfnResponse from '../../../interfaces/CfnResponse';
import migrations from '../../../migrations';

async function runMigrations(): Promise<CfnResponse> {
    try {
        const connection = await establishDbConnection({
            migrations,
        });

        console.info(connection.options);

        await connection.runMigrations();

        return {
            status: 'SUCCESS',
        };
    } catch (err) {
        console.error(err.message);

        return {
            status: 'FAILED',
        };
    }
}

export const handler = middy(runMigrations)
    .use(doNotWaitForEmptyEventLoop())
    .use(cfnCustomResourceWrapper());
