import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import entities from './entities';
import migrations from './migrations';

require('dotenv').config();

const config: PostgresConnectionOptions = {
    entities,
    migrations,
    name: 'default',
    type: 'postgres',
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PWD,
    database: process.env.DB_NAME,
    synchronize: false,
    logging: process.env.ENV === 'development',
    migrationsRun: true,
    cli: {
        entitiesDir: 'src/entities/',
        subscribersDir: 'src/subscribers/',
        migrationsDir: 'src/migrations/',
    },
};

module.exports = config;
